package com.yang.newRetail.service;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Component
public class CousumerFallbackFactory implements FallbackFactory<OpenFeginService>{
    @Override
    public OpenFeginService create(Throwable throwable) {
        return new OpenFeginService() {

            @Override
            public Map<String, Object> calculateProfit(@RequestParam(name = "orderId") String orderId) throws ParseException{
                Map<String, Object> msg = new HashMap<String, Object>();
                msg.put("massage", "客户端提供了降级信息，服务暂时关闭");
                msg.put("code", 500);
                return msg;
            };

            @Override
            public Map<String, Object> purchaseGiftBag(@RequestParam(name = "vipOrderId") String vipOrderId) throws ParseException{
                Map<String, Object> msg = new HashMap<String, Object>();
                msg.put("massage", "客户端提供了降级信息，服务暂时关闭");
                msg.put("code", 500);
                return msg;
            };
        };
    }
}
