package com.yang.newRetail.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.Map;

@FeignClient(name = "nacos-provider-newretail", fallbackFactory = CousumerFallbackFactory.class)
public interface OpenFeginService {

    @PostMapping("/provider/calculateProfit")
    Map<String, Object> calculateProfit(@RequestParam(name = "orderId") String orderId) throws ParseException;

    @PostMapping("/provider/purchaseGiftBag")
    Map<String, Object> purchaseGiftBag(@RequestParam(name = "vipOrderId") String vipOrderId) throws ParseException;
}
