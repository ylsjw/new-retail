package com.yang.newRetail.controller;

import com.yang.newRetail.service.OpenFeginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.text.ParseException;
import java.util.Map;

@RestController
public class NewRetailController {
    @Autowired
    private OpenFeginService openFeginService;

    @PostMapping("/consumer/calculateProfit")
    Map<String, Object> calculateProfit(@RequestParam(name = "orderId") String orderId) throws ParseException{
        return openFeginService.calculateProfit(orderId);
    };

    @PostMapping("/consumer/purchaseGiftBag")
    Map<String, Object> purchaseGiftBag(@RequestParam(name = "vipOrderId") String vipOrderId) throws ParseException{
        return openFeginService.purchaseGiftBag(vipOrderId);
    };
}