package com.yang.newRetail.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.newRetail.dao.StoreMapper;
import com.yang.newRetail.model.Store;
import org.springframework.stereotype.Service;

@Service
public class StoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements StoreService{

}
