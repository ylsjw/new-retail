package com.yang.newRetail.service;

import lombok.Data;

@Data
public class Colon {
    private Float code;

    public Colon(Float code) {
        this.code = code;
    }

    public static void printValur(Object str) {
        System.out.println("print value : " + str);
    }
}
