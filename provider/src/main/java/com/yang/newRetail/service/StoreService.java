package com.yang.newRetail.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.newRetail.model.Store;

public interface StoreService extends IService<Store> {

}
