package com.yang.newRetail.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.newRetail.dao.UserMapper;
import com.yang.newRetail.model.Orders;
import com.yang.newRetail.model.Store;
import com.yang.newRetail.model.Student;
import com.yang.newRetail.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, Users> implements UserService {

    @Resource
    private StoreService storeService;

    @Resource
    private OrderCopyService orderCopyService;

//    @Resource
////    @Autowired
//    private StoreServiceImpl storeService;

    public void fn(){
        Store s = storeService.lambdaQuery()
                .eq(Store::getUserId, "54574587567356")
                .eq(Store::getAgentId, "18947823903497535")
                .orderByAsc(Store::getUpdateAt)
                .one();
        System.out.println("s: " + s);
    }

    public void func(int pageIndex, int pageSize){
//        String[] array = new String[10];
        List<String> array = new ArrayList<>();
        array.add("54574587567354");
        array.add("54574587567358");
        array.add("54574587567355");

        List<Users> users = lambdaQuery().between(Users::getIntegral, 30, 100)
                .likeLeft(Users::getName, "Rust")
                .in(Users::getId, array)
                .exists("select name from users where agent_level > 3")
                .orderByDesc(Users::getCreateAt)
                .list();
        System.out.println("users: " + users);

        fn();

        List<Orders> os = orderCopyService.lambdaQuery().ge(Orders::getCollectionPrice, 10.54).list();
        System.out.println("os: " + os);

        Page<Store> page = new Page<>(pageIndex, pageSize);
//        LambdaQueryWrapper<Store> queryWrapper = Wrappers.<Store>lambdaQuery();
        LambdaQueryWrapper<Store> wrapper = Wrappers.lambdaQuery(Store.class);

//        wrapper.eq(Store::getAgentId, "");

        wrapper.orderByDesc(Store::getCreateAt);

        IPage<Store> ips = storeService.page(page, wrapper);

        System.out.println("store list: " + ips.getRecords());

        Page<Orders> opage = new Page<>(pageIndex, pageSize);
        System.out.println("order list: " + orderCopyService.lambdaQuery().page(opage).getRecords());

        AtomicReference<BigDecimal> tax = new AtomicReference<>(BigDecimal.ZERO);

        BigDecimal fee = new BigDecimal(10.65);

        List<Student> students = Arrays.asList(
                new Student(1, "张三", 20, 29.8, new BigDecimal(3.54)),
                new Student(2, "李四", 25, 29.5, new BigDecimal(4.54)),
                new Student(3, "赵武", 23, 30.8, new BigDecimal(5.54)),
                new Student(4, "王六", 25, 31.8, new BigDecimal(4.54)),
                new Student(5, "李大", 19, 29.8, new BigDecimal(7.54)),
                new Student(6, "徐二", 17, 21.8, new BigDecimal(8.54)),
                new Student(7, "周元", 20, 13.8, new BigDecimal(3.54))
        );
        BigDecimal reduce = students.stream().map(Student::getAlonePrice).reduce(BigDecimal.ZERO, BigDecimal::add);

//        BigDecimal reduce = new BigDecimal("2.03");

        tax.set(fee.multiply(reduce).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
        System.out.println("tax/fee: " + fee.multiply(reduce) + " " + tax);

        AtomicReference<BigDecimal> transport = new AtomicReference<>(BigDecimal.ZERO);

        Map<Integer, List<Student>> skuMap = students.stream().collect(Collectors.groupingBy(Student::getAge));
//        AtomicBoolean flag = new AtomicBoolean(true);
        for (Map.Entry<Integer, List<Student>> entry : skuMap.entrySet()) {
            Integer size = entry.getValue().size();
            transport.accumulateAndGet(fee.multiply(BigDecimal.valueOf(size)), BigDecimal::add);
        }
        System.out.println("transport: " + transport.get() + " " + transport.get().compareTo(BigDecimal.ZERO));

        BigDecimal strA = new BigDecimal("100");
        BigDecimal strB = new BigDecimal("0.05");
        System.out.println("strA + strB = " + strA.multiply(strB));

        AtomicBoolean flag = new AtomicBoolean(false);
        orderCopyService.lambdaQuery().eq(Orders::getUserName, "C").oneOpt().ifPresent(transportResource -> {
            flag.set(true);
            System.out.println("flag: " + flag.get());
        });
    }
}

