package com.yang.newRetail.service;

import com.yang.newRetail.dao.*;
import com.yang.newRetail.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class SalesManagementProfitService {

    @Autowired
    private VipOrderMapper vipOrderMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AgentMapper agentMapper;

    @Autowired
    private StoreMapper storeMapper;

    @Autowired
    private SalesManagementAwardMapper salesManagementAwardMapper;

    @Autowired
    private MembershipLevelMapper membershipLevelMapper;

    @Autowired
    private ManagementExpenseMapper managementExpenseMapper;

    private static final ReentrantLock lock = new ReentrantLock();

    public void changeBinding(Users user, Users superiorUser, VipOrders vipOrder){
        if(vipOrder.getTypeId() == 1){
            if(superiorUser.getVipLevel() < 1){
                Users superiorUserAgain = userMapper.selectById(superiorUser.getRecommendedUserId());
                changeBinding(user, superiorUserAgain, vipOrder);
            }else{
                user.setRecommendedUserId(superiorUser.getId());
                user.setRecommendedUserName(superiorUser.getName());
                user.setBoundStoreId(superiorUser.getBoundStoreId());
                user.setBoundStoreName(superiorUser.getBoundStoreName());
                userMapper.updateById(user);
            }
        }else if (vipOrder.getTypeId() == 2){
            if(superiorUser.getVipLevel() < 2){
                Users superiorUserAgain = userMapper.selectById(superiorUser.getRecommendedUserId());
                changeBinding(user, superiorUserAgain, vipOrder);
            }else{
                user.setRecommendedUserId(superiorUser.getId());
                user.setRecommendedUserName(superiorUser.getName());
                user.setBoundStoreId(superiorUser.getBoundStoreId());
                user.setBoundStoreName(superiorUser.getBoundStoreName());
                userMapper.updateById(user);
            }
        }
    }

    public void agentClassification(Agent agent, VipOrders vipOrder, float managementAmount){

        lock.lock();

        ManagementExpense managementExpense = new ManagementExpense();
        managementExpense.setUserId(agent.getUserId());
        managementExpense.setUserName(agent.getUserName());
        managementExpense.setOrderId(vipOrder.getId());
        managementExpense.setAmount(managementAmount);
        managementExpenseMapper.insert(managementExpense);

        lock.unlock();

        if(!agent.getUserId().equals("fb98b8c0-a406-11eb-b85c-5405db357c9f")){
            if (!agent.getSuperiorAgencyId().equals("")){
                Agent superiorAgent = agentMapper.selectById(agent.getSuperiorAgencyId());
                agentClassification(superiorAgent, vipOrder, managementExpense.getAmount()/2);
            }
        }
    }

    public void vipClassification(Users superiorUser, VipOrders vipOrder, float awardAmount){

        lock.lock();

        SalesManagementAward salesManagementAward = new SalesManagementAward();
        salesManagementAward.setVipLevel(superiorUser.getVipLevel());
        salesManagementAward.setUserId(superiorUser.getId());
        salesManagementAward.setUserName(superiorUser.getName());
        salesManagementAward.setOrderId(vipOrder.getId());
        salesManagementAward.setAmount(awardAmount);
        salesManagementAwardMapper.insert(salesManagementAward);

        lock.unlock();

        if(superiorUser.getVipLevel() == 1 || superiorUser.getVipLevel() == 2){
            Users superiorUserAgain = userMapper.selectById(superiorUser.getRecommendedUserId());
            if(superiorUserAgain.getAgentLevel() != 0 && superiorUserAgain.getVipLevel() != 3){
                Agent superiorUserAgent = agentMapper.queryAgentByStoreId(superiorUserAgain.getBoundStoreId());
                agentClassification(superiorUserAgent, vipOrder, salesManagementAward.getAmount()/2);
            }
        }

        if (superiorUser.getVipLevel() == 3){
            Store changeBoundStore = storeMapper.selectById(superiorUser.getBoundStoreId());
            Agent agent = agentMapper.selectById(changeBoundStore.getAgentId());
            agentClassification(agent, vipOrder, salesManagementAward.getAmount()/2);
        }else{
            Users superiorUserAgain = userMapper.selectById(superiorUser.getRecommendedUserId());
            vipClassification(superiorUserAgain, vipOrder, salesManagementAward.getAmount()/2);
        }

//        Store store = storeMapper.selectById(order.getStoreId());
//        if(superiorUser.getVipLevel() == 1 || superiorUser.getVipLevel() == 2){
//            Users superiorUserAgain = userMapper.selectById(superiorUser.getRecommendedUserId());
//            if(superiorUserAgain.getAgentLevel() != 0 && superiorUserAgain.getVipLevel() != 3){
//                Agent agent = agentMapper.selectById(store.getAgentId());
//                agentClassification(agent, vipOrder, salesManagementAward.getAmount()/2);
//            }
//        }
//
//        if (superiorUser.getVipLevel() == 3){
//            Agent agent = agentMapper.selectById(store.getAgentId());
//            agentClassification(agent, vipOrder, salesManagementAward.getAmount()/2);
//        }else{
//            Users superiorUserAgain = userMapper.selectById(superiorUser.getRecommendedUserId());
//            vipClassification(superiorUserAgain, vipOrder, salesManagementAward.getAmount()/2);
//        }
    }

    @Transactional
    public Map<String, Object> purchaseGiftBag(String vipOrderId){

        VipOrders vipOrder = vipOrderMapper.selectById(vipOrderId);
        Users user = userMapper.selectById(vipOrder.getUserId());
        Users superiorUser = userMapper.selectById(user.getRecommendedUserId());

        changeBinding(user, superiorUser, vipOrder);

        if(vipOrder.getTypeId() == 1) {
            MembershipLevel membershipLevel1 = membershipLevelMapper.queryMembershipByLevel(1);
            vipClassification(superiorUser, vipOrder, (float) membershipLevel1.getSalesManagementAwardAmount()/2);
        }else if (vipOrder.getTypeId() == 2){
            MembershipLevel membershipLevel2 = membershipLevelMapper.queryMembershipByLevel(2);
            vipClassification(superiorUser, vipOrder, (float) membershipLevel2.getSalesManagementAwardAmount()/2);
        }

        Map<String, Object> msg = new HashMap<String, Object>();
        msg.put("massage", "vip礼包分润计算成功");
        msg.put("code", 200);
        return msg;
    }
}
