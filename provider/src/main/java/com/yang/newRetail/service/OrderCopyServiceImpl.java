package com.yang.newRetail.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.newRetail.dao.OrderMapper;
import com.yang.newRetail.model.Orders;
import org.springframework.stereotype.Service;

@Service
public class OrderCopyServiceImpl extends ServiceImpl<OrderMapper, Orders> implements OrderCopyService{

}
