package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.VipOrders;
import org.springframework.stereotype.Repository;

@Repository
public interface VipOrderMapper extends BaseMapper<VipOrders> {
}
