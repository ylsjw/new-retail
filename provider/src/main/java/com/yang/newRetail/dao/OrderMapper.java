package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.Orders;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper extends BaseMapper<Orders> {
}
