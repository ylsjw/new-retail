package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.ManagementExpense;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagementExpenseMapper extends BaseMapper<ManagementExpense> {
}
