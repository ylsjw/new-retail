package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.RetailProfit;
import org.springframework.stereotype.Repository;

@Repository
public interface RetailProfitMapper extends BaseMapper<RetailProfit> {
}
