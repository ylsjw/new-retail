package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.LikeRecord;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeRecordMapper extends BaseMapper<LikeRecord> {
}
