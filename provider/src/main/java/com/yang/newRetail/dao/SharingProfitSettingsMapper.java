package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.MembershipLevel;
import com.yang.newRetail.model.SharingProfitSettings;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface SharingProfitSettingsMapper extends BaseMapper<SharingProfitSettings> {
    @Select("select * from sharing_profit_settings limit 1")
    SharingProfitSettings querySharingProfitSettings();
}
