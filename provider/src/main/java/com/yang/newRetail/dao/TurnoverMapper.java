package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.Turnover;
import org.springframework.stereotype.Repository;

@Repository
public interface TurnoverMapper extends BaseMapper<Turnover> {
}
