package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.MembershipLevel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface MembershipLevelMapper extends BaseMapper<MembershipLevel> {

    @Select("select * from membership_level where vip_level = #{vipLevel}")
    MembershipLevel queryMembershipByLevel(int vipLevel);

}
