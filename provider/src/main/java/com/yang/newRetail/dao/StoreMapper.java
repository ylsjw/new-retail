package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.Store;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreMapper extends BaseMapper<Store> {
}
