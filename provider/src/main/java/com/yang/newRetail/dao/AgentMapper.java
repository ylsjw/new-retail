package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.Agent;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentMapper extends BaseMapper<Agent> {

    @Select("select * from agent where user_id = #{userId}")
    Agent queryAgentByUserId(String userId);

    @Select("select * from agent a inner join store s on a.id = s.agent_id where s.id = #{storeId}")
    Agent queryAgentByStoreId(String storeId);

}
