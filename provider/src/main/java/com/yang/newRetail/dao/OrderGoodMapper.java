package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.OrderGoods;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderGoodMapper extends BaseMapper<OrderGoods> {
}
