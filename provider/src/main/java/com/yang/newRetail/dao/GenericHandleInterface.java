package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public interface GenericHandleInterface<M, T> {

    public String getMName(M m);

    public T query(String id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

    public List<T> queryAll(Wrapper<T> queryWrapper) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;
}
