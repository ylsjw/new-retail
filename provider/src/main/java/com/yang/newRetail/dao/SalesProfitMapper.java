package com.yang.newRetail.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.newRetail.model.SalesProfit;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesProfitMapper extends BaseMapper<SalesProfit> {
}
