package com.yang.newRetail.global;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

@Component
public class Val {

    private static final ArrayBlockingQueue<Map<String, Object>> resultQueue = new ArrayBlockingQueue<>(100);

    public void resultQueuePut(Map<String, Object> message) throws InterruptedException {
        resultQueue.put(message);
    }

    public Map<String, Object> resultQueueTake() throws InterruptedException {
        return resultQueue.take();
    }
}
