package com.yang.newRetail.initialize;

import com.alibaba.fastjson.JSON;
import com.yang.newRetail.global.Val;
import com.yang.newRetail.service.WebSocketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class WsCommandLineRunnerImpl implements CommandLineRunner{

    @Autowired
    private Val val;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("WsCommandLineRunner init...");

//        new Thread(()-> {
//            while (true){
//                Map<String, Object> result = null;
//                try {
//                    result = val.resultQueueTake();
//                } catch (InterruptedException e) {
//                    System.out.println("resultQueue take error");
//                    throw new RuntimeException(e);
//                }
//
//                Map<String, Object> finalResult = new HashMap<>();
//                finalResult.put("code", 1000);
//                finalResult.put("message", JSON.toJSONString(result));
//
//                WebSocketService.sendMessageByUser((Integer) result.get("userId"), JSON.toJSONString(finalResult));
//            }
//        }).start();
    }
}
