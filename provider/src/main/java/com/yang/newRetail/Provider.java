package com.yang.newRetail;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//@MapperScan({"com.gitee.sunchenbin.mybatis.actable.dao.*"} )
//@ComponentScan("com.gitee.sunchenbin.mybatis.actable.manager.*")
@MapperScan("com.yang.newRetail.dao")
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
public class Provider {
    public static void main(String[] args) {
        SpringApplication.run(Provider.class, args);
    }
}