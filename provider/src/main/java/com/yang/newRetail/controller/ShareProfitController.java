package com.yang.newRetail.controller;

import com.yang.newRetail.service.ShareProfitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ShareProfitController {

    @Autowired
    private ShareProfitService shareProfitService;

    @PostMapping("/provider/calculateProfit")
    public Map<String, Object> calculateProfit(@RequestParam(name = "orderId") String orderId){

        return shareProfitService.calculateProfit(orderId);
    }
}
