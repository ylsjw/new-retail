package com.yang.newRetail.controller;

import com.yang.newRetail.service.SalesManagementProfitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SalesManagementProfitController {

    @Autowired
    private SalesManagementProfitService salesManagementProfitService;

    @PostMapping("/provider/purchaseGiftBag")
    public Map<String, Object> purchaseGiftBag(@RequestParam(name = "vipOrderId") String vipOrderId){

        return salesManagementProfitService.purchaseGiftBag(vipOrderId);
    }
}
