package com.yang.newRetail.controller;

import com.yang.newRetail.model.Orders;
import com.yang.newRetail.service.OrderService;
import com.yang.newRetail.service.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class orderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/provider/placeAnOrder")
    public Map<String, Object> placeAnOrder(@RequestBody Orders order) throws InterruptedException {
        System.out.println("order："+order);
        return orderService.placeAnOrder(order);
    }

    @PostMapping("/provider/sendStartingPointPraise")
    public Map<String, Object> sendStartingPointPraise(@RequestParam(name = "giveActivityId") String giveActivityId) throws InterruptedException {
        System.out.println("giveActivityId："+giveActivityId);
        return orderService.sendStartingPointPraise(giveActivityId);
    }

    @PostMapping("/provider/giveTheThumbsUp")
    public Map<String, Object> giveTheThumbsUp(@RequestParam(name = "giveActivityId") String giveActivityId) throws InterruptedException {
        System.out.println("giveActivityId："+giveActivityId);
        return orderService.giveTheThumbsUp(giveActivityId);
    }

    @PostMapping("/provider/generateSpecifications")
    public List<Map<String, Object>> generateSpecifications(@RequestBody Map<String, List<String>> specMap) {
        return orderService.generateSpecifications(specMap);
    }

    @PostMapping("/provider/generateSpecificationsMultiple")
    public List<Map<String, Object>> generateSpecificationsMultiple(@RequestBody List<Map<String, List<String>>> specMapList) {
        return orderService.generateSpecificationsMultiple(specMapList);
    }

    @PostMapping("/provider/asynchronousComputing")
    public Integer asynchronousComputing() throws InterruptedException {
        return orderService.asynchronousComputing();
    }

    @PostMapping("/provider/send/{userId}")
    public String setResult(@PathVariable Integer userId) throws InterruptedException {
        return orderService.setResult(userId);
    }

    //推送数据接口
    @PostMapping("/provider/push/{cid}")
    public Map<String, Object> pushToWeb(@PathVariable Integer cid, @RequestParam(name = "message") String message) {
        WebSocketService.sendMessageByUser(cid, message);

        WebSocketService.sendAllMessage(message);

        Map<String, Object> result = new HashMap<>();
        result.put("code", 1000);
        result.put("message", "success");
        result.put("data", null);

        return result;
    }
}
