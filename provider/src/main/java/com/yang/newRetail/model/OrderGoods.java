package com.yang.newRetail.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
public class OrderGoods {
    @TableId(type = IdType.ASSIGN_UUID)
    private String Id;
    private String OrderId;
    private String GoodId;
    @TableField(fill = FieldFill.INSERT)
    private Date CreateAt;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date UpdateAt;

//    public OrderGoods(String orderId, String goodId){
//        this.OrderId = orderId;
//        this.GoodId = goodId;
//    }

//    public OrderGoods() {
//
//    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getGoodId() {
        return GoodId;
    }

    public void setGoodId(String goodId) {
        GoodId = goodId;
    }

    public Date getCreateAt() {
        return CreateAt;
    }

    public void setCreateAt(Date createAt) {
        CreateAt = createAt;
    }

    public Date getUpdateAt() {
        return UpdateAt;
    }

    public void setUpdateAt(Date updateAt) {
        UpdateAt = updateAt;
    }
}
