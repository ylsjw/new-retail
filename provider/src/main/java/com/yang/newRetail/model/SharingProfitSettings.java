package com.yang.newRetail.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SharingProfitSettings {
    @TableId(type = IdType.ASSIGN_UUID)
    private String Id;
    private int CorrespondingProductFraction;
    private int UnboundStores;
    private int AgencyPercentage;
    private int CorrespondingGoldCoin;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getCorrespondingProductFraction() {
        return CorrespondingProductFraction;
    }

    public void setCorrespondingProductFraction(int correspondingProductFraction) {
        CorrespondingProductFraction = correspondingProductFraction;
    }

    public int getUnboundStores() {
        return UnboundStores;
    }

    public void setUnboundStores(int unboundStores) {
        UnboundStores = unboundStores;
    }

    public int getAgencyPercentage() {
        return AgencyPercentage;
    }

    public void setAgencyPercentage(int agencyPercentage) {
        AgencyPercentage = agencyPercentage;
    }

    public int getCorrespondingGoldCoin() {
        return CorrespondingGoldCoin;
    }

    public void setCorrespondingGoldCoin(int correspondingGoldCoin) {
        CorrespondingGoldCoin = correspondingGoldCoin;
    }
}
