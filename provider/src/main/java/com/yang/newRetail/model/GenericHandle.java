package com.yang.newRetail.model;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yang.newRetail.dao.GenericHandleInterface;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenericHandle<M, T> implements GenericHandleInterface<M, T> {

    private M m;
    private T t;

    public GenericHandle(M m, T t){
        this.m = m;
        this.t = t;
    }

    public M getM() {
        return m;
    }

    public void setM(M m) {
        this.m = m;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public void created() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException, InstantiationException {
        Class cm = m.getClass();
        Class ct = t.getClass();
//        Method mm = cm.getMethod("insert", t.getClass());
        Method mm = cm.getMethod("insert", Object.class);
        mm.invoke(m, t);

        Field storeId = ct.getDeclaredField("storeId");
        Field userId = ct.getDeclaredField("userId");
        Field userName = ct.getDeclaredField("userName");
        Field goodList = ct.getDeclaredField("goodList");
        Field collectionPrice = ct.getDeclaredField("collectionPrice");
        Field amount = ct.getDeclaredField("amount");

        //打开私有访问
        storeId.setAccessible(true);
        userId.setAccessible(true);
        userName.setAccessible(true);
        goodList.setAccessible(true);
        collectionPrice.setAccessible(true);
        amount.setAccessible(true);

        Constructor ctc = ct.getConstructor();
        Object nt = ctc.newInstance();

        storeId.set(nt,"GFDHFGJGHJ988523543");
        userId.set(nt,"HGKJLJKL43756876");
        userName.set(nt,"泛型新增 属性 generic");
        goodList.set(nt,"23546547,745235346,6347457456,525345");
        collectionPrice.set(nt,(float)534.65);
        amount.set(nt,(float) 409.74);

        Method amm = cm.getMethod("insert", Object.class);
        amm.invoke(m, nt);
    }

    public void delete(String id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cm = m.getClass();
        Method mm = cm.getMethod("deleteById", Serializable.class);
        mm.invoke(m, id);
    }

    public void update() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cm = m.getClass();
        Class tc = t.getClass();
//        Method mm = cm.getMethod("updateById", t.getClass());
        Method mm = cm.getMethod("updateById", Object.class);
        mm.invoke(m, t);
    }

    @Override
    public String getMName(M m) {
        return m.getClass().getName();
    }

    @Override
    public T query(String id) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cm = m.getClass();
        Method mm = cm.getMethod("selectById", Serializable.class);
        T qt = (T) mm.invoke(m, id);
        return qt;
    }

    @Override
    public List<T> queryAll(Wrapper<T> queryWrapper) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cm = m.getClass();
        Method mm = cm.getMethod("selectList", Wrapper.class);
        List<T> qts = (List<T>) mm.invoke(m, queryWrapper);
        return qts;
    }

    public <N,E> E queryByGenericMethod(String id, E e,N n) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cn= n.getClass();
        Method mn = cn.getMethod("selectById", Serializable.class);
        E qt = (E) mn.invoke(n, id);
        return qt;
    }

    public <N, E> List<E> queryAllByGenericMethod(E e, N n) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cn = n.getClass();
        Method mn = cn.getMethod("selectList", Wrapper.class);
        List<E> qts = (List<E>) mn.invoke(n, new QueryWrapper<E>());
        return qts;
    }

    public <N, E> List<? extends E> play(E e, N n) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<? extends E> ee = new ArrayList<>();
        Map<? super N, ? extends E> sem = new HashMap<>();

        return ee;
    }

    public <N, E> GenericHandleInterface<? extends M, ? super T> test(GenericHandle<? extends M, ? super T> gh, E e, N n) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        return gh;
    }

    public <K, V> Map<K, V> testMap(K key, V value) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Map<K, V> m = new HashMap<>();
        m.put(key, value);
        return m;
    }
}
