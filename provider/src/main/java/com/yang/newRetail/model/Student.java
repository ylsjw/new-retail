package com.yang.newRetail.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Student {
    private int Id;
    private String Name;
    private int Age;
    private Double Balance;
    private BigDecimal AlonePrice;

    public Student(int id, String name, int age, Double balance, BigDecimal alonePrice) {
        Id = id;
        Name = name;
        Age = age;
        Balance = balance;
        AlonePrice = alonePrice;
    }
}
