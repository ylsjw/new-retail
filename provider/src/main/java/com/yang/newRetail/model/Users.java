package com.yang.newRetail.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Users {
    @TableId(type = IdType.ASSIGN_UUID)
    private String Id;
    private String Name;
    private int VipLevel;
    private String VipLevelName;
    private int AgentLevel;
    private String AgentLevelName;
    private String BoundStoreId;
    private String BoundStoreName;
    private String recommendedUserId;
    private String recommendedUserName;
    private float Integral;
    @TableField(fill = FieldFill.INSERT)
    private Date CreateAt;
    @TableField(fill = FieldFill.DEFAULT)
    private Date UpdateAt;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getVipLevel() {
        return VipLevel;
    }

    public void setVipLevel(int vipLevel) {
        VipLevel = vipLevel;
    }

    public String getVipLevelName() {
        return VipLevelName;
    }

    public void setVipLevelName(String vipLevelName) {
        VipLevelName = vipLevelName;
    }

    public int getAgentLevel() {
        return AgentLevel;
    }

    public void setAgentLevel(int agentLevel) {
        AgentLevel = agentLevel;
    }

    public String getAgentLevelName() {
        return AgentLevelName;
    }

    public void setAgentLevelName(String agentLevelName) {
        AgentLevelName = agentLevelName;
    }

    public String getBoundStoreId() {
        return BoundStoreId;
    }

    public void setBoundStoreId(String boundStoreId) {
        BoundStoreId = boundStoreId;
    }

    public String getBoundStoreName() {
        return BoundStoreName;
    }

    public void setBoundStoreName(String boundStoreName) {
        BoundStoreName = boundStoreName;
    }

    public String getRecommendedUserId() {
        return recommendedUserId;
    }

    public void setRecommendedUserId(String recommendedUserId) {
        this.recommendedUserId = recommendedUserId;
    }

    public String getRecommendedUserName() {
        return recommendedUserName;
    }

    public void setRecommendedUserName(String recommendedUserName) {
        this.recommendedUserName = recommendedUserName;
    }

    public float getIntegral() {
        return Integral;
    }

    public void setIntegral(float integral) {
        Integral = integral;
    }

    public Date getCreateAt() {
        return CreateAt;
    }

    public void setCreateAt(Date createAt) {
        CreateAt = createAt;
    }

    public Date getUpdateAt() {
        return UpdateAt;
    }

    public void setUpdateAt(Date updateAt) {
        UpdateAt = updateAt;
    }
}
