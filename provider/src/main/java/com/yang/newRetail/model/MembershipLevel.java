package com.yang.newRetail.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MembershipLevel {
    @TableId(type = IdType.ASSIGN_UUID)
    private String Id;
    private int VipLevel;
    private int SharingProfitRatio;
    private int SalesManagementAwardAmount;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getVipLevel() {
        return VipLevel;
    }

    public void setVipLevel(int vipLevel) {
        VipLevel = vipLevel;
    }

    public int getSharingProfitRatio() {
        return SharingProfitRatio;
    }

    public void setSharingProfitRatio(int sharingProfitRatio) {
        SharingProfitRatio = sharingProfitRatio;
    }

    public int getSalesManagementAwardAmount() {
        return SalesManagementAwardAmount;
    }

    public void setSalesManagementAwardAmount(int salesManagementAwardAmount) {
        SalesManagementAwardAmount = salesManagementAwardAmount;
    }
}
