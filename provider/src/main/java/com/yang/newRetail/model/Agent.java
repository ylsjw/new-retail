package com.yang.newRetail.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Agent {
    @TableId(type = IdType.ASSIGN_UUID)
    private String Id;
    private String Name;
    private String UserId;
    private String UserName;
    private String SuperiorAgencyId;
    @TableField(fill = FieldFill.INSERT)
    private Date CreateAt;
    @TableField(fill = FieldFill.DEFAULT)
    private Date UpdateAt;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getSuperiorAgencyId() {
        return SuperiorAgencyId;
    }

    public void setSuperiorAgencyId(String superiorAgencyId) {
        SuperiorAgencyId = superiorAgencyId;
    }

    public Date getCreateAt() {
        return CreateAt;
    }

    public void setCreateAt(Date createAt) {
        CreateAt = createAt;
    }

    public Date getUpdateAt() {
        return UpdateAt;
    }

    public void setUpdateAt(Date updateAt) {
        UpdateAt = updateAt;
    }
}
