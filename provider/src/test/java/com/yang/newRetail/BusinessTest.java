package com.yang.newRetail;

import com.yang.newRetail.service.OrderService;
import com.yang.newRetail.service.UserServiceImpl;
import com.yang.newRetail.utils.LowercaseUnderline;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
public class BusinessTest {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void testo() throws InterruptedException {
        Map<String, Object> res = orderService.sendStartingPointPraise("u");
        System.out.println("res：" + res);
    }

    @Test
    public void testLower() throws InterruptedException {
        String s = LowercaseUnderline.upperCharToUnderLine("OrderId");
        System.out.println("s：" + s);
    }

    @Test
    public void testLambda(){
        orderService.exec();
    }

    @Test
    public void testStreaming(){
        orderService.streaming();
    }

    @Test
    public void testCrud(){
        userService.func(3, 2);
    }
}
